package application;

public class Mesto {
	
	String nazev;
	double souradniceX;
	double souradniceY;
	
	public Mesto(String nazev, double souradniceX, double souradniceY) {
		this.nazev = nazev;
		this.souradniceX = souradniceX;
		this.souradniceY = souradniceY;
	}

	public double getSouradniceX() {
		return souradniceX;
	}

	public void setSouradniceX(double souradniceX) {
		this.souradniceX = souradniceX;
	}

	public double getSouradniceY() {
		return souradniceY;
	}

	public void setSouradniceY(double souradniceY) {
		this.souradniceY = souradniceY;
	}
	
	
}
