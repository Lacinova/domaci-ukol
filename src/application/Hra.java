package application;

import java.util.ArrayList;
import java.util.List;

import javafx.beans.InvalidationListener;
import javafx.beans.Observable;

public class Hra {
    private List<Mesto> seznamMest;     
    private boolean konecHry = false;
    private int celkoveSkore;
    private int dilciSkore;


    public Hra() {
    		Mesto praha = new Mesto("Praha", 15.10, 14.40);
    		Mesto cb = new Mesto("České Budějovice", 14.98, 14.48);
    	
        seznamMest = new ArrayList<>();
        seznamMest.add(praha);
        seznamMest.add(cb);
    }

    public void prepocitejSouradnice() {
    		for (int i = 0; i < seznamMest.size(); i++) {
    			Mesto mesto = seznamMest.get(i);
			double novaSouradniceX = mesto.getSouradniceX() / 10 + 50;
			double novaSouradniceY = mesto.getSouradniceY() / 10 + 50;
    			mesto.setSouradniceX(novaSouradniceX);
    			mesto.setSouradniceY(novaSouradniceY);
    		}
    }
    
    public int getCelkoveSkore() {
		return celkoveSkore;
	}

	public int getDilciSkore() {
		return dilciSkore;
	}

	public void pridejBody() {
    		
    }
    
    public boolean konecHry() {
        return konecHry;
    }
      
 
    void setKonecHry(boolean konecHry) {
        this.konecHry = konecHry;
    }
     
     
    
}

